/*
 Pluto V3R API V.0.6
 */

#pragma once

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif



typedef enum port_s {
    Pin1 = 1,
    Pin2,
    Pin3,
    Pin4,
    Pin5

} port_e;


#ifdef __cplusplus
}
#endif

