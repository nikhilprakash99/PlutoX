/*
 Pluto V3R API V.0.6
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint32_t microsT(void);



class Timer {

private:

uint32_t time;
uint32_t loopTime;



public:

    Timer();

    // allowed timer range 20ms-5000ms
    bool start(uint32_t time);



};



#ifdef __cplusplus
}
#endif
