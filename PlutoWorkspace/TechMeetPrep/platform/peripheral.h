/*
 Pluto V3R API V.0.6
 */

#pragma once

#include <stdint.h>
#include "specifiers.h"

#ifdef __cplusplus
 extern "C" {
#endif


 typedef enum
 { GPIO_AIN = 0x0,
   GPIO_IN_FLOATING = 0x04,
   GPIO_IPD = 0x28,
   GPIO_IPU = 0x48,
   GPIO_Out_OD = 0x14,
   GPIO_Out_PP = 0x10,
   GPIO_AF_OD = 0x1C,
   GPIO_AF_PP = 0x18
 }GPIOMode_e;


 /* GPIO Pins: Pin1, Pin2, Pin3, Pin4, Pin5  */

 class GPIO_P {
     public:

         void init(port_e pin,GPIOMode_e mode);


         void setLow(port_e pin);


         void setHigh(port_e pin);

 };





 /* ADC Pins: Pin2, Pin3, Pin4, Pin5  */

class ADC_P {
    public:

        void init(port_e pin);


        uint16_t read(port_e pin);
};




/* Timer Pins: Pin1, Pin4, Pin5  */

class Timer_P {
    public:

     void init(port_e pin_number);


      void setPWM(uint16_t value);


    private:

       class TimerImpl;
       TimerImpl* timerimpl_;


};





extern ADC_P ADC;

extern GPIO_P GPIO;

#ifdef __cplusplus
}
#endif


