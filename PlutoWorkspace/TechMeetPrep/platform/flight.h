/*
 Pluto V3R API V.0.6
 */

#pragma once
#include <stdint.h>



#ifdef __cplusplus
extern "C" {
#endif
typedef enum inclinations {
	AG_ROLL = 0, AG_PITCH, AG_YAW

} inclinations_e;



typedef enum flip_directions{

	BACK_FLIP=0

} flip_directions_e;




class Flight_P {

public:

	bool isReadyToFlip(void);
	bool isInFlip(void);
	bool setFlip(flip_directions_e direction);
	int16_t getAngle(inclinations_e angle);



};






extern Flight_P Flight;

#ifdef __cplusplus
}
#endif
