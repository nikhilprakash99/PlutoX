/*
 Pluto V3R API V.0.6
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum rc_channels {

	RC_ROLL = 0, RC_PITCH, RC_YAW, RC_THROTTLE

} rc_channels_e;


typedef enum f_status {

	    FS_Accel_Gyro_Calibration = 0,
		FS_Mag_Calibration,
		FS_Low_battery,
		FS_LowBattery_inFlight,
		FS_Crash,
		FS_Signal_loss,
		FS_Not_ok_to_arm,
		FS_Ok_to_arm,
		FS_Armed






} f_status_e;



typedef enum failsafe {

	LOW_BATTERY=0,
	INFLIGHT_LOW_BATTERY,
	CRASH,
	ALL

} failsafe_e;






class Control_P {

public:

	bool isOkToArm(void);
	bool isArmed(void);
	bool arm(void);
	bool disArm(void);
    void setRC(rc_channels_e channel, int16_t value);
	int16_t getRC(rc_channels_e channel);
	void disableFlightStatus(bool disable);
	bool checkFlightStatus(f_status_e status);
	f_status_e getFlightStatus(void);
    void setFailsafe(failsafe_e failsafe, bool active);
    void setUserLoopFrequency(uint16_t frequency); // allowed timer range 0ms-300ms default is 100ms



};

extern Control_P Control;

#ifdef __cplusplus
}
#endif

