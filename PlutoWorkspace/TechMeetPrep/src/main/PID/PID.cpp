/*
 * PID.cpp
 *
 *  Created on: 07-Dec-2018
 *      Author: nikhilprakash99
 *      Email : nikhilprakash99@gmail.com
 */

#include "PID.h"
#include "utils.h"

PID::PID(double* Input, double* Output, double* Setpoint,double Kp, double Ki, double Kd )
{
    myOutput = Output;
    myInput = Input;
    mySetpoint = Setpoint;

    PID::SetOutputLimits(1000,2000);				
    SampleTime = 1000000;

    PID::SetTunings(Kp, Ki, Kd);

    lastTime = microsT()-SampleTime;
}

bool PID::Compute()
{
   uint32_t now = microsT();
   uint32_t timeChange = (now - lastTime);
   if(timeChange>=SampleTime)
   {
      double input = *myInput;
      double error = *mySetpoint - input;
      double dInput = (input - lastInput);

      outputSum = kp * error;
      outputSum+= ki * error;
      outputSum-= kd * dInput;

	  if(outputSum > outMax) outputSum = outMax;
       else if(outputSum < outMin) outputSum = outMin;
	  *myOutput = outputSum;

      lastInput = input;
      lastTime = now;
	    
	  return true;
   }
   else return false;
}

void PID::SetTunings(double Kp, double Ki, double Kd)
{
   dispKp = Kp; dispKi = Ki; dispKd = Kd;

   double SampleTimeInSec = ((double)SampleTime)/1000000;
   kp = Kp;
   ki = Ki * SampleTimeInSec;
   kd = Kd / SampleTimeInSec;
}

void PID::SetSampleTime(uint32_t NewSampleTime)
{
   if (NewSampleTime > 0)
   {
      double ratio  = (double)NewSampleTime
                      / (double)SampleTime;
      ki *= ratio;
      kd /= ratio;
      SampleTime = (uint32_t)NewSampleTime;
   }
}

void PID::SetOutputLimits(double Min, double Max)
{
   outMin = Min;
   outMax = Max;

   if(*myOutput > outMax) *myOutput = outMax;
	else if(*myOutput < outMin) *myOutput = outMin;

   if(outputSum > outMax) outputSum= outMax;
    else if(outputSum < outMin) outputSum= outMin;
}

double PID::GetKp(){ return  dispKp;}
double PID::GetKi(){ return  dispKi;}
double PID::GetKd(){ return  dispKd;}
